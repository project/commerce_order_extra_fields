Adds new (extra) fields to Commerce order display.

Added fields are:
1. Order status
2. Customer IP
3. Payment method
4. Shipping method
5. Order mail
6. Link to the user who made the order

Requirements Pseudo field module.
